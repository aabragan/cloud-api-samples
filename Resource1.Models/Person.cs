﻿using System;

namespace Resource1.Models
{
    public class Person
    {
        public Guid TenantUid { get; set; }

        public int Id { get; set; }

        public Guid Uid { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zipcode { get; set; }

        public string Cellphone { get; set; }

        public string Mainphone { get; set; }

        public string Email { get; set; }

        public string CreatedOn { get; set; }

        public string ModifiedOn { get; set; }      
    }
}
