﻿using System.Collections.Generic;
using Platform.Shared.Interfaces;

namespace Resource1.GetById
{
    public class ActionHandlerFactory : IActionHandlerFactory
    {
        public IActionHandler CreateNew(Dictionary<string, object> options)
        {
            return new ActionHandler();
        }
    }
}