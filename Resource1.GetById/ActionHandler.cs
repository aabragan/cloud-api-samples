﻿using System;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Newtonsoft.Json.Linq;
using Platform.Shared.Interfaces;

namespace Resource1.GetById
{
    public class ActionHandler : IActionHandler
    {
        protected string ActionName => "getbyid";

        public async Task<bool> Verify(dynamic data, ILambdaContext context)
        {
            return data != Guid.Empty;
        }

        public async Task<dynamic> Execute(dynamic data, IDataProvider dataProvider, ILambdaContext context)
        {
            // pre-commit business logic

            // store the validated record in the backend datastores
            dynamic result = await dataProvider.Execute(ActionName, data, context);
            JObject outputData = null;
            if (result != null)
                outputData = JObject.FromObject(result);

            // post-commit business logic

            return outputData;
        }
    }
}