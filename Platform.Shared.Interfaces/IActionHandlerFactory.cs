﻿using System.Collections.Generic;

namespace Platform.Shared.Interfaces
{
    public interface IActionHandlerFactory
    {
        IActionHandler CreateNew(Dictionary<string, object> options);
    }
}