﻿using System.Threading.Tasks;
using Amazon.Lambda.Core;

namespace Platform.Shared.Interfaces
{
    public interface IActionHandler
    {
        /// <summary>
        /// Execute the business logic operation for this action and return the results
        /// </summary>
        /// <param name="data">The data to process</param>
        /// <param name="dataProvider">The data provider to use to process the data</param>
        /// <param name="context">The execution context</param>
        /// <returns>The processed data</returns>
        Task<dynamic> Execute(dynamic data, IDataProvider dataProvider, ILambdaContext context);

        /// <summary>
        /// Verifies that the data sent is what is required by the business logic
        /// </summary>
        /// <param name="data">The data to process</param>
        /// <param name="context">The execution context</param>
        /// <returns>The processed data</returns>
        Task<bool> Verify(dynamic data, ILambdaContext context);
    }
}