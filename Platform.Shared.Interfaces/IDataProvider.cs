﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Amazon.Runtime;
using Newtonsoft.Json.Linq;

namespace Platform.Shared.Interfaces
{
    public interface IDataProvider
    {
        Task<dynamic> Execute(string operation, dynamic data, ILambdaContext context);
    }
}