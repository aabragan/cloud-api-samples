﻿using System.Collections.Generic;

namespace Platform.Shared.Interfaces
{
    public interface IDataProviderFactory
    {
        IDataProvider CreateNew(Dictionary<string, object> options);
    }
}