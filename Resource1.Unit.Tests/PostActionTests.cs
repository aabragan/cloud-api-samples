using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;
using FluentAssertions;
using Platform.Shared.Models;
using Resource1.Post.Actions;

namespace Resource1.Unit.Tests
{
    public class PostActionTests
    {
        [Fact]
        public void TestToUpperFunction()
        {
            var input = new WorkflowData();
            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            
            var result = function.FunctionHandler(input, context);
            result.Should().NotBeNull();
            result.Should().BeOfType<WorkflowData>();
        }
    }
}
