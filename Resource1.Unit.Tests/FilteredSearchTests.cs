using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Platform.Shared.Models;
using Platform.Shared.Interfaces;
using Resource1.Models;
using Resource1.FilteredSearch;

namespace Resource1.Unit.Tests
{
    public class FilteredSearchTests
    {
        #region Properties
        private readonly Person _person;
        private readonly Guid _initialStepId;
        private readonly Guid _primaryKey;

        #endregion


        #region ctor
        public FilteredSearchTests()
        {
            _person = HelperMethods.CreateSamplePerson();
            _initialStepId = new Guid("2FA22096-8178-4631-8435-6E4D1B3AF8BE");
            _primaryKey = new Guid("3533B80D-B1A6-49FA-A21A-D8DE7FA41E92");
        }
        #endregion

        #region Success Tests
        [Fact]
        public async void Function_Succeeds_And_Returns_WorkflowData()
        {
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var input = new WorkflowData();
            // Invoke the lambda function and confirm the string was upper cased.
            var lambdaContextMock = new TestLambdaContext();
            var function = ScaffoldFunction(null, settings);
            // var context = new TestLambdaContext();

            var result = await function.FunctionHandler(input, lambdaContextMock);
            // base assertions
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();
        }

        [Fact]
        public async void Function_Succeeds_And_Returns_WorkflowData_With_AllObjects()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData()
            {
                InitialStepId = _initialStepId
            };

            var personList = HelperMethods.GetPersonList(_person, _primaryKey);
            var function = ScaffoldFunction(personList, settings);
            // var context = new TestLambdaContext();

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = result.Data as List<object>;
            data.Should().NotBeNull().And.BeOfType<List<object>>();
            data.Any().Should().BeTrue("the collection has records");
            var firstElement = data.First() as JObject;
            firstElement.Property("Uid").Should().NotBeNull("Uid is the primary key of the record and is required");
            firstElement["Uid"].Value<Guid>().Should().Be(_primaryKey, "the primary key should be assigned when the record is stored to the datas store");
        }

        #endregion

        #region Failure Tests
        [Fact]
        public void Function_Throws_Exception_With_MissingSettings()
        {
            var personList = HelperMethods.GetPersonList(_person, _primaryKey);
            var ex = Assert.Throws<ArgumentException>(() => ScaffoldFunction(personList, null));

            ex.Message.Should().Be("settings", "settings were not passed into the method");
        }

        [Fact]
        public async void Function_Throws_Exception_And_Returns_WorkflowData_With_Error_Flag()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData()
            {
                InitialStepId = _initialStepId
            };

            // Invoke the lambda function and confirm the string was upper cased.
            var dataProviderMock = new Mock<IDataProvider>();
            dataProviderMock.SetupAllProperties();
            dataProviderMock.Setup(p => p.Execute(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<ILambdaContext>()))
                .Throws<Exception>();               

            var dataProviderFactoryMock = new Mock<IDataProviderFactory>();
            dataProviderFactoryMock.SetupAllProperties();
            dataProviderFactoryMock.Setup(p => p.CreateNew(It.IsAny<Dictionary<string, object>>()))
                .Returns(dataProviderMock.Object);

            // execute actual business logic
            var actionHandlerFactory = new ActionHandlerFactory();

            var function = new Function(dataProviderFactoryMock.Object, actionHandlerFactory, settings);

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();
            
            result.Error.Should().BeTrue("an error occurred searching for results");
            
            var data = result.Data as List<object>;
            data.Should().BeNull();
        }

        [Fact]
        public async void Function_Fails_And_Returns_WorkflowData_With_Null_Data_And_ErrorFlag()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData()
            {
                InitialStepId = _initialStepId
            };

            var function = ScaffoldFunction(null, settings);

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = result.Data as List<object>;
            data.Should().BeNull();
            result.Error.Should().BeTrue();
        }

        [Fact]
        public async void Function_Fails_Because_Of_Find_Fails_And_Returns_WorkflowData_With_ErrorFlag()
        {
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData
            {
                InitialStepId = _initialStepId,
                Data = _person
            };
            var taskSource = new TaskCompletionSource<dynamic>();
            taskSource.SetResult(new List<Dictionary<string, object>>());

            // Invoke the lambda function and confirm the string was upper cased.
            var lambdaContextMock = new TestLambdaContext();
            var dataProviderMock = new Mock<IDataProvider>();
            dataProviderMock.SetupAllProperties();
            dataProviderMock.Setup(p => p.Execute(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<ILambdaContext>()))
                .Returns(taskSource.Task);

            var dataProviderFactoryMock = new Mock<IDataProviderFactory>();
            dataProviderFactoryMock.SetupAllProperties();
            dataProviderFactoryMock.Setup(p => p.CreateNew(It.IsAny<Dictionary<string, object>>()))
                .Returns(dataProviderMock.Object);

            // execute actual business logic
            var actionHandlerFactory = new ActionHandlerFactory();

            var function = new Function(dataProviderFactoryMock.Object, actionHandlerFactory, settings);

            var result = await function.FunctionHandler(input, lambdaContextMock);
            // base assertions
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();
            result.Error.Should().BeTrue("the input data was empty");
        }


        #endregion

        #region Filter Tests

        [Fact]
        public async void Function_Succeeds_And_Returns_WorkflowData_With_FilteredObjects()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var queryString = HelperMethods.GetQueryStringVariables();
            queryString["fields"] = "Uid,LastName";

            var input = new WorkflowData
            {
                InitialStepId = _initialStepId,
                Request = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
                {
                    {"header", HelperMethods.GetHeaderVariables()},
                    {"path", HelperMethods.GetPathVariables()},
                    {"querystring", queryString}
                }
            };

            var personList = HelperMethods.GetPersonList(_person, _primaryKey);
            var function = ScaffoldFunction(personList, settings);

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = result.Data as List<object>;
            data.Should().NotBeNull().And.BeOfType<List<object>>();
            data.Any().Should().BeTrue("the collection has records");
            var firstElement = data.First() as JObject;
            //firstElement.ContainsKey("Uid").Should().BeTrue("Uid is the primary key of the record and is required");
            firstElement.Should().HaveCount(2, "only Uid and LastName were requested");
            firstElement["Uid"].Value<Guid>().Should().Be(_primaryKey, "the primary key should be assigned when the record is stored to the datas store");
            firstElement["LastName"].Value<string>().Should().Be(_person.LastName, "the last name should match the expected value");

        }


        [Fact]
        public async void Function_Succeeds_And_Returns_WorkflowData_With_Count_Only()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var queryString = HelperMethods.GetQueryStringVariables();
            queryString["count"] = true;

            var input = new WorkflowData
            {
                InitialStepId = _initialStepId,
                Request = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
                {
                    {"header", HelperMethods.GetHeaderVariables()},
                    {"path", HelperMethods.GetPathVariables()},
                    {"querystring", queryString}
                }
            };

            var personList = HelperMethods.GetPersonList(_person, _primaryKey);
            var function = ScaffoldFunction(personList, settings);
            // var context = new TestLambdaContext();

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = (int)result.Data;
            data.Should().Be(1, "only one record is returned");
        }



        [Fact]
        public async void Function_Succeeds_And_Returns_WorkflowData_With_Objects()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData
            {
                InitialStepId = _initialStepId,
                Request = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
                {
                    {"header", HelperMethods.GetHeaderVariables()},
                    {"path", HelperMethods.GetPathVariables()},
                    {"querystring", HelperMethods.GetQueryStringVariables()}
                }
            };

            var personList = HelperMethods.GetPersonList(_person, _primaryKey);
            var propertyCount = ((JObject) personList.First()).Count;
            var function = ScaffoldFunction(personList, settings);
            // var context = new TestLambdaContext();

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = result.Data as List<object>;
            data.Should().NotBeNull().And.BeOfType<List<object>>();
            data.Any().Should().BeTrue("the collection has records");
            var firstElement = data.First() as JObject;
            //firstElement.ContainsKey("Uid").Should().BeTrue("Uid is the primary key of the record and is required");
            firstElement.Should().HaveCount(propertyCount, "all properties were implicitly requested");
            firstElement["Uid"].Value<Guid>().Should().Be(_primaryKey, "the primary key should be assigned when the record is stored to the datas store");
            firstElement["LastName"].Value<string>().Should().Be(_person.LastName, "the last name should match the expected value");
        }

        #endregion

        #region Helper Methods
        private static Function ScaffoldFunction(List<object> personList, Dictionary<string, object> settings)
        {
            var taskSource = new TaskCompletionSource<dynamic>();
            taskSource.SetResult(personList);

            // Invoke the lambda function and confirm the string was upper cased.
            var dataProviderMock = new Mock<IDataProvider>();
            dataProviderMock.SetupAllProperties();
            dataProviderMock.Setup(p => p.Execute(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<ILambdaContext>()))
                .Returns(taskSource.Task);

            var dataProviderFactoryMock = new Mock<IDataProviderFactory>();
            dataProviderFactoryMock.SetupAllProperties();
            dataProviderFactoryMock.Setup(p => p.CreateNew(It.IsAny<Dictionary<string, object>>()))
                .Returns(dataProviderMock.Object);

            // execute actual business logic
            var actionHandlerFactory = new ActionHandlerFactory();

            var function = new Function(dataProviderFactoryMock.Object, actionHandlerFactory, settings);
            return function;
        }


        #endregion
    }
}
