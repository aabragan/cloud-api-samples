using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Platform.Shared.Models;
using Platform.Shared.Interfaces;
using Resource1.Models;
using Resource1.DeleteById;

namespace Resource1.Unit.Tests
{
    public class DeleteByIdTests
    {
        #region Properties
        private readonly Person _person;
        private readonly Guid _initialStepId;
        private readonly Guid _primaryKey;

        #endregion

        #region ctor
        public DeleteByIdTests()
        {
            _person = HelperMethods.CreateSamplePerson();
            _initialStepId = new Guid("2FA22096-8178-4631-8435-6E4D1B3AF8BE");
            _primaryKey = new Guid("3533B80D-B1A6-49FA-A21A-D8DE7FA41E92");
        }
        #endregion

        #region Success Tests
        [Fact]
        public async void Function_Succeeds_And_Returns_WorkflowData_With_True_Data_When_Id_Provided()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData
            {
                InitialStepId = _initialStepId,
                Data = _primaryKey
            };

            var personJson = JsonConvert.SerializeObject(_person);

            var updatedPerson = JsonConvert.DeserializeObject<Dictionary<string, object>>(personJson);
            updatedPerson["Uid"] = _primaryKey; // set primary key

            var function = ScaffoldFunction(updatedPerson, settings);
            // var context = new TestLambdaContext();

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = (bool)result.Data;

            data.Should().BeTrue("the record was successfully deleted, if it was found");
            result.Error.Should().BeFalse("the record was successfully deleted, if it was found");
        }


        [Fact]
        public async void Function_Succeeds_And_Returns_WorkflowData_With_False_Data_When_Bad_Id_Provided()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData
            {
                InitialStepId = _initialStepId,
                Data = _primaryKey
            };

            var function = ScaffoldFunction(null, settings);
            // var context = new TestLambdaContext();

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = (bool)result.Data;

            data.Should().BeFalse("the record was not found");
            result.Error.Should().BeTrue("the record was not found");
        }

        #endregion

        #region Failure Tests
        [Fact]
        public void Function_Throws_Exception_With_MissingSettings()
        {
            var personJson = JsonConvert.SerializeObject(_person);

            var updatedPerson = JsonConvert.DeserializeObject<Dictionary<string, object>>(personJson);

            
            updatedPerson["Uid"] = _primaryKey; // set primary key


            var ex = Assert.Throws<ArgumentException>(() => ScaffoldFunction(updatedPerson, null));

            ex.Message.Should().Be("settings", "settings were not passed into the method");
        }

        [Fact]
        public async void Function_Throws_Exception_And_Returns_WorkflowData_With_Error_Flag()
        {
            var lambdaContextMock = new TestLambdaContext();
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData()
            {
                InitialStepId = _initialStepId,
                Data = _primaryKey
            };

            // Invoke the lambda function and confirm the string was upper cased.
            var dataProviderMock = new Mock<IDataProvider>();
            dataProviderMock.SetupAllProperties();
            dataProviderMock.Setup(p => p.Execute(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<ILambdaContext>()))
                .Throws<Exception>();               

            var dataProviderFactoryMock = new Mock<IDataProviderFactory>();
            dataProviderFactoryMock.SetupAllProperties();
            dataProviderFactoryMock.Setup(p => p.CreateNew(It.IsAny<Dictionary<string, object>>()))
                .Returns(dataProviderMock.Object);

            // execute actual business logic
            var actionHandlerFactory = new ActionHandlerFactory();

            var function = new Function(dataProviderFactoryMock.Object, actionHandlerFactory, settings);

            var result = await function.FunctionHandler(input, lambdaContextMock);
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();
            
            result.Error.Should().BeTrue("an error occurred searching for results");
            
            var data = (bool)result.Data;
            data.Should().BeFalse("no records were deleted because an error occurred");
        }

        [Fact]
        public async void Function_Fails_Because_Of_Invalid_Input_And_Returns_WorkflowData_With_ErrorFlag()
        {
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var input = new WorkflowData();

            // Invoke the lambda function and confirm the string was upper cased.
            var lambdaContextMock = new TestLambdaContext();
            var function = ScaffoldFunction(null, settings);
            // var context = new TestLambdaContext();

            var result = await function.FunctionHandler(input, lambdaContextMock);
            // base assertions
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();

            var data = (bool)result.Data;

            data.Should().BeFalse("the input data was empty");
            result.Error.Should().BeTrue("the input data was empty");
        }

        [Fact]
        public async void Function_Fails_Because_Of_Delete_Fails_And_Returns_WorkflowData_With_ErrorFlag()
        {
            var settings = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            var input = new WorkflowData
            {
                InitialStepId = _initialStepId,
                Data = _person
            };

            var taskSource = new TaskCompletionSource<dynamic>();
            taskSource.SetResult(null);

            // Invoke the lambda function and confirm the string was upper cased.
            var lambdaContextMock = new TestLambdaContext();
            var dataProviderMock = new Mock<IDataProvider>();
            dataProviderMock.SetupAllProperties();
            dataProviderMock.Setup(p => p.Execute(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<ILambdaContext>()))
                .Returns(taskSource.Task);

            var dataProviderFactoryMock = new Mock<IDataProviderFactory>();
            dataProviderFactoryMock.SetupAllProperties();
            dataProviderFactoryMock.Setup(p => p.CreateNew(It.IsAny<Dictionary<string, object>>()))
                .Returns(dataProviderMock.Object);

            // execute actual business logic
            var actionHandlerFactory = new ActionHandlerFactory();

            var function = new Function(dataProviderFactoryMock.Object, actionHandlerFactory, settings);

            var result = await function.FunctionHandler(input, lambdaContextMock);
            // base assertions
            result.Should().NotBeNull().And.BeOfType<WorkflowData>();
            result.Error.Should().BeTrue("the input data was empty");
        }


        #endregion

        #region Helper Methods
        private static Function ScaffoldFunction(Dictionary<string, object> person, Dictionary<string, object> settings)
        {
            var taskSource = new TaskCompletionSource<dynamic>();
            taskSource.SetResult(person);

            // Invoke the lambda function and confirm the string was upper cased.
            var dataProviderMock = new Mock<IDataProvider>();
            dataProviderMock.SetupAllProperties();
            dataProviderMock.Setup(p => p.Execute(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<ILambdaContext>()))
                .Returns(taskSource.Task);

            var dataProviderFactoryMock = new Mock<IDataProviderFactory>();
            dataProviderFactoryMock.SetupAllProperties();
            dataProviderFactoryMock.Setup(p => p.CreateNew(It.IsAny<Dictionary<string, object>>()))
                .Returns(dataProviderMock.Object);

            // execute actual business logic
            var actionHandlerFactory = new ActionHandlerFactory();

            var function = new Function(dataProviderFactoryMock.Object, actionHandlerFactory, settings);
            return function;
        }

        #endregion    
    }
}
