﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Linq;
using Resource1.Models;

namespace Resource1.Unit.Tests
{
    public static class HelperMethods
    {
        public static Person CreateSamplePerson()
        {
            return new Person
            {
                Id = 7,
                Address1 = "111 Main St.",
                Address2 = "Apt 22",
                Cellphone = "615-555-6666",
                City = "Columbus",
                CreatedOn = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                Email = "bob@aol.com",
                FirstName = "Bob",
                LastName = "Builder",
                Mainphone = "614-555-7777",
                ModifiedOn = DateTime.UtcNow.ToString(CultureInfo.InvariantCulture),
                State = "OH",
                TenantUid = Guid.NewGuid(),
                Zipcode = "43215"
            };
        }

        public static JObject GetPathVariables()
        {
            return new JObject();
        }

        public static JObject GetQueryStringVariables()
        {
            return new JObject();
        }

        public static JObject GetHeaderVariables()
        {
            return new JObject();
        }

        public static List<object> GetPersonList(Person person, Guid primaryKey)
        {
            var updatedPerson = GetPerson(person, primaryKey);

            var personList = new List<object> { updatedPerson };
            return personList;
        }

        public static JObject GetPerson(Person person, Guid primaryKey)
        {
            var updatedPerson = JObject.FromObject(person);
            updatedPerson["Uid"] = primaryKey; // set primary key

            return updatedPerson;

        }

    }
}