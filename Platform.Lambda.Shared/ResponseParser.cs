﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FastMember;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Platform.Lambda.Shared
{
    /// <summary>
    /// Response parser.  
    /// This class will parse request responses and convert to dynamics based on fields requested.
    /// </summary>
    public static class ResponseParser
    {
        /// <summary>
        /// Packages return data to the caller using the filters specified
        /// </summary>
        /// <param name="source">The complete result to return</param>
        /// <param name="filters">The filters provided</param>
        /// <returns>A filtered dataset of one or more objects</returns>
        public static dynamic FilterResponseData(object source, Dictionary<string, object> filters = null)
        {
            if (source == null)
            {
                return null;
            }

            var enumerableData = source as IEnumerable;

            var isEnumerable = enumerableData != null;

            // if the item is a single
            var type = source.GetType();
            // may be different if we have a string  and a list of strings
            var sourceType = source.GetType();

            var isDictionary = type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(IDictionary<,>) || source is IDictionary);

            // if it is enumerable get the type of the elements
            if (isEnumerable && !isDictionary)
            {
                var arguments = type.GetGenericArguments();
                if (arguments != null && arguments.Any())
                    type = type.GetGenericArguments()[0];
            }

            var itemCount = isEnumerable ? enumerableData.Count() : 1;

            // Return a single int, count, if desired
            if (filters != null && filters.ContainsKey("count"))
            {
                return itemCount;
            }

            if (source is JArray items)
            {
                return ProcessJArray(items, filters);
            }
            if (source is JObject item)
            {
                return ProcessJObject(item, filters);
            }
            if (type.IsPrimitive() && type == sourceType)
                return Formatter(type, source);
            if (isDictionary)
                return ProcessDictionary(enumerableData, filters);
            if (isEnumerable)
                return ProcessList(enumerableData, filters);

            return ProcessObject(source, type, filters);
        }

        private static dynamic ProcessJArray(JArray items, Dictionary<string, object> filters)
        {
            JArray result = new JArray();
            foreach (var item in items)
            {
                switch (item.Type)
                {
                    case JTokenType.Array:
                    {
                        var value = ProcessJArray(item as JArray, filters);
                        result.Add(item);
                        break;
                    }
                    case JTokenType.Object:
                    {
                        var value = ProcessJObject(item as JObject, filters);
                        result.Add(value);
                        break;
                    }
                    default:
                        result.Add(item);
                        break;
                }
            }

            return result;
        }

        private static dynamic ProcessJObject(JObject item, Dictionary<string, object> filters)
        {
            JObject result = new JObject();
            var fields = filters != null && filters.ContainsKey("fields") ? new HashSet<string>(filters["fields"].ToString().Split(','), StringComparer.OrdinalIgnoreCase) : new HashSet<string>();
            foreach (var kvp in item)
            {
                if (fields.Any() && !fields.Contains(kvp.Key)) continue;

                switch (kvp.Value.Type)
                {
                    case JTokenType.Array:
                        break;
                    case JTokenType.Object:
                        var value = ProcessJObject(kvp.Value as JObject, filters);
                        result.Add(kvp.Key, value);
                        break;
                    default:
                        result.Add(kvp.Key, kvp.Value);
                        break;
                }
            }

            return result;
        }

        private static object ProcessDictionary(dynamic source, Dictionary<string, object> filters = null)
        {
            var result = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var fields = filters != null && filters.ContainsKey("fields") ? new HashSet<string>(filters["fields"].ToString().Split(','), StringComparer.OrdinalIgnoreCase) : new HashSet<string>();
            foreach (dynamic item in source)
            {
                string key = item.Key.ToString();
                if (!fields.Any() || fields.Contains(key))
                    result[key] = FilterResponseData(item.Value, filters);
            }
            return result;
        }

        private static object ProcessList(IEnumerable source, Dictionary<string, object> filters = null)
        {
            var result = new List<object>();
            foreach (var item in source)
            {
                var processedItem = FilterResponseData(item, filters);
                result.Add(processedItem);
            }
            return result;
        }

        /// <summary>
        /// Given the object to return, its type and the HandlerData (mainly for its Fields specification), create a new Dictionary of just the desired properties and their values.
        /// </summary>
        /// <param name="source">The object to package.</param>
        /// <param name="type">The object's type.</param>
        /// <param name="filters">The filters provided</param>
        /// <returns>Dictionary of property names and the value.</returns>
        private static object ProcessObject(object source, Type type, Dictionary<string, object> filters = null)
        {
            if ((source as Exception) != null)
            {
                // If we are processing an Exception, no further processing. Otherwise it will not serialize correctly.
                return source;
            }

            var typeAccessor = TypeAccessor.Create(type);

            // case-insensitive, O(1) lookup for column names.
            var memberSet = typeAccessor.GetMembers();
            var memberList = memberSet.Select(member => member.Name).ToList();
            var members = new HashSet<string>(memberList, StringComparer.OrdinalIgnoreCase);
            var fieldList = members;

            var fields = filters != null && filters.ContainsKey("fields") ? filters["fields"].ToString().Split(',') : new string[] { };
            if (fields.Any())
                fieldList = new HashSet<string>(fields.Where(field => members.Contains(field)), StringComparer.OrdinalIgnoreCase);

            var fieldsToProcess = fieldList.Intersect(members, StringComparer.OrdinalIgnoreCase);

            var responseData = GetFilteredFields(source, fieldsToProcess, memberSet, typeAccessor, filters);

            return responseData;
        }

        private static IDictionary<string, object> GetFilteredFields(object source, IEnumerable<string> fieldList, MemberSet memberSet, TypeAccessor typeAccessor, Dictionary<string, object> filters = null)
        {
            var filteredFields = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var objectAccessor = ObjectAccessor.Create(source);

            foreach (var fieldName in fieldList)
            {
                var member = memberSet.FirstOrDefault(item => item.Name.Equals(fieldName, StringComparison.OrdinalIgnoreCase));
                if (member == null) continue;

                if (!IsFieldToReturn(member)) continue;
                if (objectAccessor[member.Name] != null)
                {
                    filteredFields[member.Name] = FilterResponseData(typeAccessor[source, member.Name], filters);
                }
            }
            return filteredFields;
        }

        private static bool IsFieldToReturn(Member member) => !member.IsDefined(typeof(JsonIgnoreAttribute));

        private static object Formatter(Type type, object source)
        {
            // all non-string values
            if (type != typeof(string) || source == null) return source;

            // only strings processed here
            var result = source.ToString();

            return result;
        }
    }

}