﻿using System;
using System.Collections.Generic;
using Platform.Shared.Interfaces;

namespace Platform.Lambda.Shared
{
    public abstract class FunctionBase
    {
        protected IDataProvider DataProvider;

        protected IActionHandler ActionHandler;

        protected abstract string FailureMessage { get; }
        protected abstract string SuccessMessage { get; }

        protected abstract Dictionary<string, object> GetDataProviderSettings();

        protected virtual void Initialize(IDataProviderFactory dataProviderFactory, IActionHandlerFactory actionHandlerFactory, Dictionary<string, object> settings)
        {
            DataProvider = dataProviderFactory.CreateNew(settings);
            ActionHandler = actionHandlerFactory.CreateNew(settings);
        }
    }
}
