﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Platform.Lambda.Shared
{
    public static class ExtensionMethods
    {
        #region Type Extensions

        private static readonly List<Type> SimpleTypes = new List<Type>
        {
            typeof(byte),
            typeof(sbyte),
            typeof(short),
            typeof(ushort),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(float),
            typeof(double),
            typeof(decimal),
            typeof(bool),
            typeof(string),
            typeof(char),
            typeof(Guid),
            typeof(DateTime),
            typeof(DateTimeOffset),
            typeof(byte[])
        };

        /// <summary>
        /// Gets a value indicating whether the Type is one the Macola "primitive" types
        /// </summary>
        /// <param name="type">The type to check</param>
        /// <returns>A value indicating whether a type is a "primitive"</returns>
        public static bool IsPrimitive(this Type type)
        {
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                var arguments = type.GetGenericArguments();
                if (arguments.Any())
                    type = type.GetGenericArguments()[0];
            }
            var isBigInteger = type.FullName == "System.Numerics.BigInteger";
            return type.IsPrimitive ||
                   type.IsEnum ||
                   SimpleTypes.Contains(type) ||
                   isBigInteger;
        }

        #endregion

        public static int Count(this IEnumerable source)
        {
            var result = 0;
            var enumerator = source.GetEnumerator();
            while (enumerator.MoveNext())
                result++;
            return result;
        }
    }
}