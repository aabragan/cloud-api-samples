﻿using System;
using System.Collections.Generic;

namespace Platform.Shared.Models
{
    public class WorkflowData
    {
        public Guid InitialStepId { get; set; }

        public string WorkflowName { get; set; }

        public string WorkflowIdentifier { get; set; }

        public dynamic Data { get; set; }

        public dynamic Metadata { get; set; }

        public dynamic Request { get; set; }

        public IEnumerable<string> Messages { get; set; }

        public bool Error { get; set; }
    }
}
