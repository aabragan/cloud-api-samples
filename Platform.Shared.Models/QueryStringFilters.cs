﻿namespace Platform.Shared.Models
{
    public class QueryStringFilters
    {
        public string Fields { get; set; }

        public string Order { get; set; }

        public bool Count { get; set; }

        public int Page { get; set; }

        public int PageSize { get; set; }
    }
}
