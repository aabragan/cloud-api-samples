﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Amazon.Lambda.Core;
using Amazon.Runtime;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Platform.Shared.Interfaces;
using Resource1.Models;

namespace Resource1.Data
{
    public class Resource1DataProvider : IDataProvider
    {
        private readonly dynamic _options;

        private readonly IDynamoDBContext _dbContext;

        public Resource1DataProvider(Dictionary<string, object> options)
        {
            _options = options;
            // Check to see if a table name was passed in through environment variables and if so 
            // add the table mapping.
            //var tableName = System.Environment.GetEnvironmentVariable(TABLENAME_ENVIRONMENT_VARIABLE_LOOKUP);
            //var tableName = options[TABLENAME_ENVIRONMENT_VARIABLE_LOOKUP].ToString();
            var tableName = "Person";
            if (!string.IsNullOrEmpty(tableName))
            {
                AWSConfigsDynamoDB.Context.TypeMappings[typeof(Person)] = new Amazon.Util.TypeMapping(typeof(Person), tableName);
            }

            var config = new DynamoDBContextConfig { Conversion = DynamoDBEntryConversion.V2 };

            this._dbContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
        }

        public async Task<dynamic> Execute(string operation, dynamic data, ILambdaContext context)
        {
            dynamic result = null;

            switch (operation)
            {
                case "create":
                    {
                        await _dbContext.SaveAsync<Person>(data);
                        result = data;

                        break;
                    }
                case "update":
                    {
                        if (data is JObject record)
                        {
                            var uid = new Guid(record["Uid"].Value<string>());

                            var current = await _dbContext.LoadAsync<Person>(uid);
                            if (current != null)
                            {
                                JObject lookup = JObject.FromObject(data);

                                JObject updatedProperties = JObject.FromObject(current);

                                foreach (var item in lookup)
                                {
                                    updatedProperties[item.Key] = item.Value;
                                }

                                result = updatedProperties.ToObject<Person>();
                                await _dbContext.SaveAsync<Person>(result);
                            }
                        }

                        break;
                    }
                case "getbyid":
                    {
                        result = await _dbContext.LoadAsync<Person>(data);
                        break;
                    }
                case "find":
                    {
                        var search = _dbContext.ScanAsync<Person>(null);
                        result = await search.GetNextSetAsync();
                        break;
                    }
                case "delete":
                    {
                        string value = data.ToString();
                        var search = _dbContext.ScanAsync<Person>(new List<ScanCondition>
                        {
                            new ScanCondition("Uid", ScanOperator.Equal, value.ToUpper())
                        });
                        var collection = await search.GetNextSetAsync();

                        result = collection.FirstOrDefault();

                        await _dbContext.DeleteAsync<Person>(result);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }

            //const string TABLENAME_ENVIRONMENT_VARIABLE_LOOKUP = "PersonTable";

            //public const string ID_QUERY_STRING_NAME = "Id";

            //private IDynamoDBContext _dbContext;

            //public Function()
            //{
            //    // Check to see if a table name was passed in through environment variables and if so 
            //    // add the table mapping.
            //    var tableName = System.Environment.GetEnvironmentVariable(TABLENAME_ENVIRONMENT_VARIABLE_LOOKUP);
            //    if(!string.IsNullOrEmpty(tableName))
            //    {
            //        AWSConfigsDynamoDB.Context.TypeMappings[typeof(Person)] = new Amazon.Util.TypeMapping(typeof(Person), tableName);
            //    }

            //    var config = new DynamoDBContextConfig { Conversion = DynamoDBEntryConversion.V2 };
            //    this._dbContext = new DynamoDBContext(new AmazonDynamoDBClient(), config);
            //}

            //public Function(IDynamoDBContext dbContext)
            //{
            //    _dbContext = dbContext;
            //}

            ///// <summary>
            ///// A Lambda function that returns back a page worth of blog posts.
            ///// </summary>
            ///// <param name="request"></param>
            ///// <param name="context"></param>
            ///// <returns>The list of blogs</returns>
            //public async Task<List<Person>> FunctionHandler(Dictionary<string, object> request, ILambdaContext context)
            //{
            //    context.Logger.LogLine("Getting persons");
            //    var search = _dbContext.ScanAsync<Person>(null);
            //    var page = await search.GetNextSetAsync();
            //    context.Logger.LogLine($"Found {page.Count} persons");

            //    return page;
            //}


            return result;
        }
    }
}
