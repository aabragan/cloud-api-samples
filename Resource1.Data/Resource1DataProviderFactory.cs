﻿using System.Collections.Generic;
using Platform.Shared.Interfaces;

namespace Resource1.Data
{
    public class Resource1DataProviderFactory : IDataProviderFactory
    {
        public IDataProvider CreateNew(Dictionary<string, object> options)
        {
            return new Resource1DataProvider(options);
        }
    }
}