using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Newtonsoft.Json.Linq;
using Platform.Lambda.Shared;
using Platform.Shared.Models;
using Platform.Shared.Interfaces;
using Resource1.Models;
using Resource1.Data;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Resource1.Update
{
    public sealed class Function : FunctionBase
    {
        protected override string FailureMessage => "Unable to update person";
        protected override string SuccessMessage => "Successfully update person";

        /// <summary>
        /// Constructor used by AWS Lambda Engine
        /// </summary>
        [ExcludeFromCodeCoverage]
        public Function()
        {
            var settings = GetDataProviderSettings();
            var dataProviderFactory = new Resource1DataProviderFactory();
            var actionHandlerFactory = new ActionHandlerFactory();
            Initialize(dataProviderFactory, actionHandlerFactory, settings);
        }

        /// <summary>
        /// Constructor used for Unit Tests
        /// </summary>
        /// <param name="dataProviderFactory">The Data Provider Factory</param>
        /// <param name="actionHandlerFactory">The Factory to provide handling of the business logic</param>
        /// <param name="settings">The settings used by the factory to create a new Data Provider</param>
        public Function(IDataProviderFactory dataProviderFactory, IActionHandlerFactory actionHandlerFactory, Dictionary<string, object> settings)
        {
            if (settings == null)
                throw new ArgumentException(nameof(settings));

            Initialize(dataProviderFactory, actionHandlerFactory, settings);
        }

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<WorkflowData> FunctionHandler(WorkflowData input, ILambdaContext context)
        {
            var messages = new List<string>();
            var result = new WorkflowData
            {
                InitialStepId = input.InitialStepId,
                Error = false
            };

            try
            {
                JObject inputData = null;
                if (input.Data != null)
                {
                    inputData = JObject.FromObject(input.Data);
                }

                if (await ActionHandler.Verify(inputData, context))
                {
                    // execute business rules here
                    dynamic processedData = await ActionHandler.Execute(inputData, DataProvider, context);

                    if (processedData == null)
                    {
                        result.Data = null;
                        result.Error = true;
                        messages.Add(FailureMessage);
                    }
                    else
                    {
                        result.Data = processedData;
                        result.Error = false;
                        messages.Add(SuccessMessage);
                    }
                }
                else
                {
                    // if something goes wrong
                    result.Data = null;
                    result.Error = true;
                    messages.Add("Unable to validate input data");
                }
            }
            catch (Exception exception)
            {
                result.Data = null;
                result.Error = true;
                messages.Add(exception.Message);
            }

            result.Messages = messages;
            return result;
        }

        [ExcludeFromCodeCoverage]
        protected override Dictionary<string, object> GetDataProviderSettings()
        {
            return new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)
            {
                {"PersonTable", "Person"}
            };
        }
    }
}
