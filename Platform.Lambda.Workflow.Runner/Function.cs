using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Runtime;
using Amazon.StepFunctions;
using Amazon.StepFunctions.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Platform.Shared.Models;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Platform.Lambda.Workflow.Runner
{
    public class Function
    {
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task<WorkflowData> FunctionHandler(WorkflowData input, ILambdaContext context)
        {
            var credentials = new EnvironmentVariablesAWSCredentials();

            var client = new AmazonStepFunctionsClient(credentials);

            var stateMachineArn = input.WorkflowIdentifier;
            if (string.IsNullOrWhiteSpace(stateMachineArn))
            {
                Dictionary<string, object> queryString = ParseQueryString(input.Request);
                if (queryString.ContainsKey("workflowname"))
                {
                    input.WorkflowName = queryString["workflowname"].ToString();
                }

                if (queryString.ContainsKey("workflowidentifier"))
                {
                    input.WorkflowIdentifier = queryString["workflowidentifier"].ToString();
                }

                if (await ValidateStateMachineArn(client, input.WorkflowIdentifier))
                {
                    stateMachineArn = input.WorkflowIdentifier;
                }

                if (string.IsNullOrWhiteSpace(stateMachineArn) && !string.IsNullOrWhiteSpace(input.WorkflowName))
                    stateMachineArn = await LookupStateMachineArn(client, input.WorkflowName);
            }

            if (!await ValidateStateMachineArn(client, stateMachineArn))
            {
                return new WorkflowData
                {
                    InitialStepId = input.InitialStepId,
                    Data = _lookup,
                    Error = true,
                    Messages = new List<string> { "Invalid Workflow Identifier specified." }
                };
            }

            var workflowInput = new WorkflowData
            {
                Data = input.Data,
                Request = input.Request
            };

            var startRequest = new StartExecutionRequest
            {
                StateMachineArn = stateMachineArn,
                Input = JsonConvert.SerializeObject(workflowInput)
            };

            var startResponse = await client.StartExecutionAsync(startRequest);
            dynamic result;
            var status = ExecutionStatus.SUCCEEDED;
            do
            {
                var describeRequest = new DescribeExecutionRequest { ExecutionArn = startResponse.ExecutionArn };
                var describeResponse = await client.DescribeExecutionAsync(describeRequest);

                if (describeResponse.Status == ExecutionStatus.RUNNING)
                    Thread.Sleep(1000);
                else
                {
                    var output = describeResponse.Output;
                    status = describeResponse.Status;
                    result = JsonConvert.DeserializeObject<Dictionary<string, object>>(output);
                    break;
                }
            } while (true);


            var response = new WorkflowData
            {
                InitialStepId = input.InitialStepId,
                Data = result,
                Error = status != ExecutionStatus.SUCCEEDED
            };
            return response;
        }

        private async Task<string> LookupStateMachineArn(AmazonStepFunctionsClient client, string requestWorkflowName)
        {
            var lookup = await BuildStateMachineLookup(client);
            return lookup[requestWorkflowName];
        }

        private Dictionary<string, string> _lookup = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        private HashSet<string> _arnLookup = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        /// <summary>
        /// TBD: This method will save to cache and pull data only once 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        private async Task<Dictionary<string, string>> BuildStateMachineLookup(AmazonStepFunctionsClient client)
        {
            _lookup = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            var nextToken = string.Empty;
            do
            {
                var request = new ListStateMachinesRequest();
                if (!string.IsNullOrWhiteSpace(nextToken))
                {
                    request.NextToken = nextToken;
                }
                var stateMachines = await client.ListStateMachinesAsync(request);
                foreach (var stateMachine in stateMachines.StateMachines)
                {
                    _lookup[stateMachine.Name] = stateMachine.StateMachineArn;
                    _arnLookup.Add(stateMachine.StateMachineArn);
                }
                nextToken = stateMachines.NextToken;
            } while (!string.IsNullOrWhiteSpace(nextToken));

            return _lookup;
        }

        public async Task<bool> ValidateStateMachineArn(AmazonStepFunctionsClient client, string arn)
        {
            if (string.IsNullOrWhiteSpace(arn)) return false;

            if (!_arnLookup.Any())
            {
                await BuildStateMachineLookup(client);
            }

            return _arnLookup.Contains(arn);
        }

        private static Dictionary<string, object> ParseQueryString(IReadOnlyDictionary<string, object> filters)
        {
            if (!filters.ContainsKey("querystring")) return null;

            var queryString = filters["querystring"] as JObject;

            return queryString == null ?
                new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase) :
                queryString.ToObject<Dictionary<string, object>>();
        }

    }
}
