﻿using System.Threading.Tasks;
using Amazon.Lambda.Core;
using Newtonsoft.Json.Linq;
using Platform.Shared.Interfaces;

namespace Resource1.FilteredSearch
{
    public class ActionHandler : IActionHandler
    {
        protected string ActionName => "find";

        public async Task<bool> Verify(dynamic data, ILambdaContext context)
        {
            return data != null;
        }

        public async Task<dynamic> Execute(dynamic data, IDataProvider dataProvider, ILambdaContext context)
        {
            // pre-commit business logic

            // retrieve list of records requested from backend datastores
            dynamic result = await dataProvider.Execute(ActionName, data, context);

            // post-commit business logic

            return result;
        }
    }
}