﻿using System.Collections.Generic;
using Platform.Shared.Interfaces;

namespace Resource1.FilteredSearch
{
    public class ActionHandlerFactory : IActionHandlerFactory
    {
        public IActionHandler CreateNew(Dictionary<string, object> options)
        {
            return new ActionHandler();
        }
    }
}